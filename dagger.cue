package todoapp

import (
	"dagger.io/dagger"

	"dagger.io/dagger/core"
	"universe.dagger.io/yarn"
)

dagger.#Plan & {
	actions: {

		// Load the source code 
		source: core.#Source & {
			path: "."
			exclude: [
				"node_modules",
				"build",
				"*.cue",
				"*.md",
				".git",
			]
			
		}

		// Build 
		build: yarn.#Script & {
			name:   "build"
			source: actions.source.output
			container: env: CI: "true"
		}

		// Code Style Checker
		style: yarn.#Script & {
			name:   "prettier-check"
			source: actions.source.output
			container: env: CI: "true"
		}

		// Linter
		lint: yarn.#Script & {
			name:   "lint"
			source: actions.source.output
			container: env: CI: "true"
		}

		// Test Runner
		test: yarn.#Script & {
			name:   "test"
			source: actions.source.output
			container: env: CI: "true"
		}
	}
}
